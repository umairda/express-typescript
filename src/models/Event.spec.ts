import Event from './Event'
import EVENTS from '../config/events'

describe('Event', function() {
  it('should set the type per the constructor arguments', function() {
    const event: Event = new Event({ type: 'LOGIN' })
    expect(event.type).toBe('LOGIN')
    expect(event.getCreated()).toBeGreaterThan(Date.now() - 100000)
  })

  describe('generateRandomEvent', function() {
    it('should generate a random event', function() {
      const event: Event = Event.generateRandomEvent()
      expect(event instanceof Event).toBeTruthy()
      expect(EVENTS.indexOf(event.type) !== -1).toBeTruthy()
    })
  })

  describe('setCreatedToOverADayAgo', function() {
    const event: Event = new Event({ type: 'LOGIN' })
    const ONE_DAY: number = 24 * 3600 * 1000
    expect(Date.now() - event.getCreated() < ONE_DAY).toBeTruthy()
    event.setCreatedToOverADayAgo()
    expect(Date.now() - event.getCreated() > ONE_DAY).toBeTruthy()
  })
})
