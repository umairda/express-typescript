import EVENTS from '../config/events'

export default class Event {
  private created: number
  public email = ''
  public type: string //should be enum if all types are known

  constructor({ email, type }: { email?: string; type: string }) {
    this.type = type
    this.created = Date.now()
    if (email) {
      this.email = email
    }
  }

  public getCreated(): number {
    return this.created
  }

  //for testing
  public setCreatedToOverADayAgo(): void {
    this.created = this.created - 25 * 3600 * 1000
  }

  static generateRandomEvent(): Event {
    return new Event({ type: EVENTS[Math.floor(EVENTS.length * Math.random())] })
  }
}
