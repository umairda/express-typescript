import { Request, Response } from 'express'
import { listAllEvents, listAllEventsWithinOneDay, listUserEvents } from './events'
import Event from '../models/Event'
import User from '../models/User'

describe('listAllEvents', function() {
  it('should list all the events from all the users', async function() {
    await User.clearUsers()
    await new User({ email: 'test', password: 'test' }).save()
    await new User({ email: 'test2', password: 'test' }).save()
    const users: User[] = await User.getAllUsers()

    await Promise.all(
      users.map((user: User) => {
        user.addEvent(new Event({ type: 'DID_SOMETHING' }))
        user.addEvent(new Event({ type: 'DID_SOMETHING_ELSE' }))
        return user.update()
      })
    )

    const req: Request = {} as Request
    const res: Response = {} as Response

    res.json = jest.fn()

    await listAllEvents(req, res)

    const events: Event[] = users.reduce((p: Event[], user: User) => [...p, ...user.events], [])

    expect(res.json).toHaveBeenCalledWith(events)
  })
})

describe('listAllEventsWithinOneDay', function() {
  it('should list all the events from all the users within one day', async function() {
    await User.clearUsers()
    await new User({ email: 'test', password: 'test' }).save()
    await new User({ email: 'test2', password: 'test' }).save()
    const users: User[] = await User.getAllUsers()

    await Promise.all(
      users.map((user: User) => {
        const event1 = new Event({ type: 'DID_SOMETHING' })
        event1.setCreatedToOverADayAgo()
        user.addEvent(event1)
        user.addEvent(new Event({ type: 'DID_SOMETHING_ELSE' }))
        return user.update()
      })
    )

    const req: Request = {} as Request
    const res: Response = {} as Response

    res.json = jest.fn()

    await listAllEventsWithinOneDay(req, res)

    const events: Event[] = users.reduce(
      (p: Event[], user: User) => [
        ...p,
        ...user.events.filter((event: Event) => event.getCreated() > Date.now() - 24 * 3600 * 1000)
      ],
      []
    )

    expect(res.json).toHaveBeenCalledWith(events)
  })
})

describe('listUserEvents', function() {
  it('should list all the events for a user', async function() {
    await User.clearUsers()
    const user = new User({
      email: 'test',
      password: 'test'
    })

    user.addEvent(new Event({ type: 'DID_SOMETHING' }))
    user.addEvent(new Event({ type: 'DID_SOMETHING_AGAIN' }))
    user.addEvent(new Event({ type: 'DID_SOMETHING_ELSE' }))

    await user.save()

    const req: Request = {} as Request
    req.params = {
      email: 'test'
    }

    const res: Response = {} as Response
    res.json = jest.fn()

    await listUserEvents(req, res)

    expect(res.json).toHaveBeenCalledWith(user.events)
  })
})
