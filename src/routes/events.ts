import { Application } from 'express'

import { listAllEvents, listAllEventsWithinOneDay, listUserEvents } from '../controllers/events'

export default (app: Application): void => {
  app.get('/v1/events/:email', listUserEvents)

  app.get('/v1/events', listAllEvents)

  app.get('/v1/events-within-one-day', listAllEventsWithinOneDay)
}
