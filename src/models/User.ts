import Event from './Event'
import presidents, { President } from '../config/presidents'

//our ephemeral database
const users: Record<string, User> = {}

export default class User {
  public created: number = Date.now()
  public email = ''
  public password = ''
  public phone = ''
  public events: Event[] = []
  private version = 1

  constructor({ email, password, phone }: { email: string; password: string; phone?: string }) {
    this.email = email
    this.password = password

    if (phone && User.validatePhone(phone)) {
      this.phone = phone
    }
  }

  public addEvent(event: Event): number {
    event.email = this.email
    this.events.push(event)
    return this.events.length
  }

  static clearUsers(): Promise<number> {
    const keys = Object.keys(users)
    keys.forEach((email: string) => {
      delete users[email]
    })
    return Promise.resolve(keys.length)
  }

  static generateRandomUser(): User {
    const firstNames = presidents.map((p: President) => p.first)
    const randomFirst: string = firstNames[Math.floor(firstNames.length * Math.random())]
    const lastNames = presidents.map((p: President) => p.last)
    const randomLast: string = lastNames[Math.floor(firstNames.length * Math.random())]
    const email = `${randomFirst}${randomLast}-${Date.now()}@test.com`
    return new User({ email, password: 'password' })
  }

  static getAllUsers(): Promise<User[]> {
    return Promise.resolve(Object.values(users))
  }

  static async getRandomUser(): Promise<User | null> {
    try {
      const allUsers = await User.getAllUsers()
      return Promise.resolve(allUsers[Math.floor(allUsers.length * Math.random())])
    } catch (err) {
      return Promise.resolve(null)
    }
  }

  static getUser(email: string): Promise<User> {
    return Promise.resolve(users[email] || null)
  }

  public getVersion(): number {
    return this.version
  }

  public save(): Promise<boolean> {
    if (this.email && this.password && !users.hasOwnProperty(this.email.toLowerCase())) {
      users[this.email] = this
      return Promise.resolve(true)
    } else {
      return Promise.resolve(false)
    }
  }

  public update(): Promise<number> {
    let updateAllowed = true
    try {
      updateAllowed = this.version + 1 > users[this.email].getVersion()
    } catch (err) {}

    if (updateAllowed) {
      this.version++
      users[this.email] = this
    }

    return Promise.resolve(this.version)
  }

  static validatePhone(phone: string): boolean {
    return phone.length === 12 && new RegExp(/[1-9][0-9]{2}-[0-9]{3}-[0-9]{4}/).test(phone)
  }
}
