import Event from '../models/Event'
import User from '../models/User'
import { generateUser, generateEvent, generateEventOneDayAgo } from './eventsGenerator'

describe('generateUser', function() {
  it('should create a user with one event: SIGN_UP', async function() {
    await User.clearUsers()
    const saved: boolean = await generateUser()
    expect(saved).toBeTruthy()
    const users: User[] = await User.getAllUsers()
    expect(users.length).toBe(1)
    expect(users[0].events[0].type).toBe('SIGN_UP')
  })
})

describe('generateEvent', function() {
  it('should create an event on a random user', async function() {
    await User.clearUsers()
    const saved: boolean = await generateUser()
    expect(saved).toBeTruthy()
    const eventGenerated: boolean = await generateEvent()
    expect(eventGenerated).toBeTruthy()
    const users: User[] = await User.getAllUsers()
    expect(users.length).toBe(1)
    expect(users[0].events.length).toBe(2)
  })
})

describe('generateEvent', function() {
  it('should create an event one day ago on a random user', async function() {
    await User.clearUsers()
    const saved: boolean = await generateUser()
    expect(saved).toBeTruthy()
    const eventGenerated: boolean = await generateEventOneDayAgo()
    expect(eventGenerated).toBeTruthy()
    const users: User[] = await User.getAllUsers()
    expect(users.length).toBe(1)
    expect(users[0].events.length).toBe(2)

    expect(
      users[0].events.reduce((p, event: Event) => {
        return p || event.getCreated() < Date.now() - 24 * 3600 * 1000
      }, false)
    ).toBeTruthy()
  })
})
