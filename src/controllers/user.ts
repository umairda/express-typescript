import { Request, Response } from 'express'
import Event from '../models/Event'
import User from '../models/User'

export const createUser = async (req: Request, res: Response): Promise<void> => {
  try {
    const user: User = new User(req.body)
    user.addEvent(new Event({ type: 'SIGN_UP' }))
    const result = await user.save()
    res.json(result)
  } catch (err) {
    res.sendStatus(500)
  }
}

export const getUser = async (req: Request, res: Response): Promise<void> => {
  try {
    const user: User | null = await User.getUser(req.params.email)

    if (!user) {
      res.json(null)
    } else {
      res.json({ ...user, password: '***' })
    }
  } catch (err) {
    res.sendStatus(500)
  }
}

export const addUserEvent = async (req: Request, res: Response): Promise<void> => {
  try {
    const user: User | null = await User.getUser(req.params.email)
    if (!user) {
      res.json({
        result: `Unable to find user with email: ${req.params.email}`
      })
    } else {
      const event: Event = new Event(req.body)
      user.addEvent(event)
      await user.update()

      res.json(user.events)
    }
  } catch (err) {
    res.sendStatus(500)
  }
}
