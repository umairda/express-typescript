import { Application } from 'express'

import { addUserEvent, createUser, getUser } from '../controllers/user'

export default (app: Application): void => {
  app.post(`/v1/user`, createUser)

  app.get(`/v1/user/:email`, getUser)

  app.post('/v1/user/:email/event', addUserEvent)
}
