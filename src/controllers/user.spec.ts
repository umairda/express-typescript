import { Request, Response } from 'express'
import { addUserEvent, createUser, getUser } from './user'
import User from '../models/User'

describe('createUser', function() {
  it('should call res.json with true if the user was created', async function() {
    const req: Request = {
      body: {
        email: 'test@test.com',
        password: 'password'
      }
    } as Request

    const res: Response = {
      json: () => {}
    } as Response

    res.json = jest.fn()

    await User.clearUsers()
    await createUser(req, res)

    expect(res.json).toHaveBeenCalledWith(true)
  })

  it('should call res.json with false if the user was NOT created', async function() {
    const req: Request = {
      body: {
        email: 'test@test.com',
        password: 'password'
      }
    } as Request

    const res: Response = {
      json: () => {}
    } as Response

    res.json = jest.fn()

    await User.clearUsers()
    const user = new User(req.body)
    await user.save()
    await createUser(req, res)

    expect(res.json).toHaveBeenCalledWith(false)
  })
})

describe('getUser', function() {
  it('should call res.json with the user if found', async function() {
    const req: Request = {} as Request
    const email = 'test@test.com'

    req.params = {
      email
    }

    const res: Response = {
      json: () => {}
    } as Response

    res.json = jest.fn()

    await User.clearUsers()
    const user = new User({ email, password: 'test' })
    await user.save()
    await getUser(req, res)

    user.password = '***'

    expect(res.json).toHaveBeenCalledWith(user)
  })

  it('should call res.json with null if the user is NOT found', async function() {
    const req: Request = {} as Request
    const email = 'test@test.com'

    req.params = {
      email
    }

    const res: Response = {
      json: () => {}
    } as Response

    res.json = jest.fn()

    await User.clearUsers()
    await getUser(req, res)

    expect(res.json).toHaveBeenCalledWith(null)
  })
})

describe('addUserEvent', function() {
  it('should find a user and add the event per the request body', async function() {
    const req: Request = {} as Request
    const email = 'test@test.com'
    const type = 'DID_SOMETHING'

    req.body = {
      type
    }

    req.params = {
      email
    }

    const res: Response = {
      json: () => {}
    } as Response

    res.json = jest.fn()

    await User.clearUsers()
    const user = new User({ email, password: 'test' })
    expect(user.events.length).toBe(0)
    await user.save()
    await addUserEvent(req, res)
    expect(user.events.length).toBe(1)

    expect(res.json).toHaveBeenCalledWith(user.events)
  })

  it('should indicate the user was not found', async function() {
    const req: Request = {} as Request
    const email = 'test@test.com'
    const type = 'DID_SOMETHING'

    req.body = {
      type
    }

    req.params = {
      email
    }

    const res: Response = {
      json: () => {}
    } as Response

    res.json = jest.fn()

    await User.clearUsers()
    await addUserEvent(req, res)

    expect(res.json).toHaveBeenCalledWith({
      result: `Unable to find user with email: ${req.params.email}`
    })
  })
})
