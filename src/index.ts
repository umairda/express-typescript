import express, { Application } from 'express'
import bodyParser from 'body-parser'
import { APP_PORT } from './config/settings'
import eventsGenerator from './helpers/eventsGenerator'
import eventsRoutes from './routes/events'
import userRoutes from './routes/user'

const app: Application = express()
app.use(bodyParser.json({ limit: '5mb' }))
app.set('json_spaces', 2)
app.locals.pretty = true

eventsRoutes(app)
userRoutes(app)

app.listen(APP_PORT, function() {
  console.info(`Process ${process.title} listening on ${APP_PORT}`)
  eventsGenerator()
})
