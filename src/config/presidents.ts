export interface President {
  first: string
  middle: string
  last: string
  number: string
  wiki: string
  tookOffice: string
  leftOffice: string
  party: string
}

export default [
  {
    first: 'George',
    middle: '',
    last: 'Washington',
    number: '1',
    name: 'George Washington',
    wiki: 'http://en.wikipedia.org/wiki/George_Washington',
    tookOffice: '30/04/1789',
    leftOffice: '4/03/1797',
    party: 'Independent '
  },
  {
    first: 'John',
    middle: '',
    last: 'Adams',
    number: '2',
    name: 'John Adams',
    wiki: 'http://en.wikipedia.org/wiki/John_Adams',
    tookOffice: '4/03/1797',
    leftOffice: '4/03/1801',
    party: 'Federalist '
  },
  {
    first: 'Thomas',
    middle: '',
    last: 'Jefferson',
    number: '3',
    name: 'Thomas Jefferson',
    wiki: 'http://en.wikipedia.org/wiki/Thomas_Jefferson',
    tookOffice: '4/03/1801',
    leftOffice: '4/03/1809',
    party: 'Democratic-Republican '
  },
  {
    first: 'James',
    middle: '',
    last: 'Madison',
    number: '4',
    name: 'James Madison',
    wiki: 'http://en.wikipedia.org/wiki/James_Madison',
    tookOffice: '4/03/1809',
    leftOffice: '4/03/1817',
    party: 'Democratic-Republican '
  },
  {
    first: 'James',
    middle: '',
    last: 'Monroe',
    number: '5',
    name: 'James Monroe',
    wiki: 'http://en.wikipedia.org/wiki/James_Monroe',
    tookOffice: '4/03/1817',
    leftOffice: '4/03/1825',
    party: 'Democratic-Republican '
  },
  {
    first: 'John',
    middle: 'Quincy',
    last: 'Adams',
    number: '6',
    name: 'John Quincy Adams',
    wiki: 'http://en.wikipedia.org/wiki/John_Quincy_Adams',
    tookOffice: '4/03/1825',
    leftOffice: '4/03/1829',
    party: 'Democratic-Republican/National Republican '
  },
  {
    first: 'Andrew',
    middle: '',
    last: 'Jackson',
    number: '7',
    name: 'Andrew Jackson',
    wiki: 'http://en.wikipedia.org/wiki/Andrew_Jackson',
    tookOffice: '4/03/1829',
    leftOffice: '4/03/1837',
    party: 'Democratic '
  },
  {
    first: 'Martin',
    middle: 'Van',
    last: 'Buren',
    number: '8',
    name: 'Martin Van Buren',
    wiki: 'http://en.wikipedia.org/wiki/Martin_Van_Buren',
    tookOffice: '4/03/1837',
    leftOffice: '4/03/1841',
    party: 'Democratic '
  },
  {
    first: 'William',
    middle: 'Henry',
    last: 'Harrison',
    number: '9',
    name: 'William Henry Harrison',
    wiki: 'http://en.wikipedia.org/wiki/William_Henry_Harrison',
    tookOffice: '4/03/1841',
    leftOffice: '4/04/1841',
    party: 'Whig'
  },
  {
    first: 'John',
    middle: '',
    last: 'Tyler',
    number: '10',
    name: 'John Tyler',
    wiki: 'http://en.wikipedia.org/wiki/John_Tyler',
    tookOffice: '4/04/1841',
    leftOffice: '4/03/1845',
    party: 'Whig'
  },
  {
    first: 'James',
    middle: 'K.',
    last: 'Polk',
    number: '11',
    name: 'James K. Polk',
    wiki: 'http://en.wikipedia.org/wiki/James_K._Polk',
    tookOffice: '4/03/1845',
    leftOffice: '4/03/1849',
    party: 'Democratic '
  },
  {
    first: 'Zachary',
    middle: '',
    last: 'Taylor',
    number: '12',
    name: 'Zachary Taylor',
    wiki: 'http://en.wikipedia.org/wiki/Zachary_Taylor',
    tookOffice: '4/03/1849',
    leftOffice: '9/07/1850',
    party: 'Whig'
  },
  {
    first: 'Millard',
    middle: '',
    last: 'Fillmore',
    number: '13',
    name: 'Millard Fillmore',
    wiki: 'http://en.wikipedia.org/wiki/Millard_Fillmore',
    tookOffice: '9/07/1850',
    leftOffice: '4/03/1853',
    party: 'Whig'
  },
  {
    first: 'Franklin',
    middle: '',
    last: 'Pierce',
    number: '14',
    name: 'Franklin Pierce',
    wiki: 'http://en.wikipedia.org/wiki/Franklin_Pierce',
    tookOffice: '4/03/1853',
    leftOffice: '4/03/1857',
    party: 'Democratic '
  },
  {
    first: 'James',
    middle: '',
    last: 'Buchanan',
    number: '15',
    name: 'James Buchanan',
    wiki: 'http://en.wikipedia.org/wiki/James_Buchanan',
    tookOffice: '4/03/1857',
    leftOffice: '4/03/1861',
    party: 'Democratic '
  },
  {
    first: 'Abraham',
    middle: '',
    last: 'Lincoln',
    number: '16',
    name: 'Abraham Lincoln',
    wiki: 'http://en.wikipedia.org/wiki/Abraham_Lincoln',
    tookOffice: '4/03/1861',
    leftOffice: '15/04/1865',
    party: 'Republican/National Union'
  },
  {
    first: 'Andrew',
    middle: '',
    last: 'Johnson',
    number: '17',
    name: 'Andrew Johnson',
    wiki: 'http://en.wikipedia.org/wiki/Andrew_Johnson',
    tookOffice: '15/04/1865',
    leftOffice: '4/03/1869',
    party: 'Democratic/National Union'
  },
  {
    first: 'Ulysses',
    middle: 'S.',
    last: 'Grant',
    number: '18',
    name: 'Ulysses S. Grant',
    wiki: 'http://en.wikipedia.org/wiki/Ulysses_S._Grant',
    tookOffice: '4/03/1869',
    leftOffice: '4/03/1877',
    party: 'Republican '
  },
  {
    first: 'Rutherford',
    middle: 'B.',
    last: 'Hayes',
    number: '19',
    name: 'Rutherford B. Hayes',
    wiki: 'http://en.wikipedia.org/wiki/Rutherford_B._Hayes',
    tookOffice: '4/03/1877',
    leftOffice: '4/03/1881',
    party: 'Republican '
  },
  {
    first: 'James',
    middle: 'A.',
    last: 'Garfield',
    number: '20',
    name: 'James A. Garfield',
    wiki: 'http://en.wikipedia.org/wiki/James_A._Garfield',
    tookOffice: '4/03/1881',
    leftOffice: '19/09/1881',
    party: 'Republican '
  },
  {
    first: 'Chester',
    middle: 'A.',
    last: 'Arthur',
    number: '21',
    name: 'Chester A. Arthur',
    wiki: 'http://en.wikipedia.org/wiki/Chester_A._Arthur',
    tookOffice: '19/09/1881',
    leftOffice: '4/03/1885',
    party: 'Republican '
  },
  {
    first: 'Grover',
    middle: '',
    last: 'Cleveland',
    number: '22',
    name: 'Grover Cleveland',
    wiki: 'http://en.wikipedia.org/wiki/Grover_Cleveland',
    tookOffice: '4/03/1885',
    leftOffice: '4/03/1889',
    party: 'Democratic '
  },
  {
    first: 'Benjamin',
    middle: '',
    last: 'Harrison',
    number: '23',
    name: 'Benjamin Harrison',
    wiki: 'http://en.wikipedia.org/wiki/Benjamin_Harrison',
    tookOffice: '4/03/1889',
    leftOffice: '4/03/1893',
    party: 'Republican '
  },
  {
    first: 'Grover',
    middle: '',
    last: 'Cleveland',
    number: '24',
    name: 'Grover Cleveland (2nd term)',
    wiki: 'http://en.wikipedia.org/wiki/Grover_Cleveland',
    tookOffice: '4/03/1893',
    leftOffice: '4/03/1897',
    party: 'Democratic '
  },
  {
    first: 'William',
    middle: '',
    last: 'McKinley',
    number: '25',
    name: 'William McKinley',
    wiki: 'http://en.wikipedia.org/wiki/William_McKinley',
    tookOffice: '4/03/1897',
    leftOffice: '14/9/1901',
    party: 'Republican '
  },
  {
    first: 'Theodore',
    middle: '',
    last: 'Roosevelt',
    number: '26',
    name: 'Theodore Roosevelt',
    wiki: 'http://en.wikipedia.org/wiki/Theodore_Roosevelt',
    tookOffice: '14/9/1901',
    leftOffice: '4/3/1909',
    party: 'Republican '
  },
  {
    first: 'William',
    middle: 'Howard',
    last: 'Taft',
    number: '27',
    name: 'William Howard Taft',
    wiki: 'http://en.wikipedia.org/wiki/William_Howard_Taft',
    tookOffice: '4/3/1909',
    leftOffice: '4/03/1913',
    party: 'Republican '
  },
  {
    first: 'Woodrow',
    middle: '',
    last: 'Wilson',
    number: '28',
    name: 'Woodrow Wilson',
    wiki: 'http://en.wikipedia.org/wiki/Woodrow_Wilson',
    tookOffice: '4/03/1913',
    leftOffice: '4/03/1921',
    party: 'Democratic '
  },
  {
    first: 'Warren',
    middle: 'G.',
    last: 'Harding',
    number: '29',
    name: 'Warren G. Harding',
    wiki: 'http://en.wikipedia.org/wiki/Warren_G._Harding',
    tookOffice: '4/03/1921',
    leftOffice: '2/8/1923',
    party: 'Republican '
  },
  {
    first: 'Calvin',
    middle: '',
    last: 'Coolidge',
    number: '30',
    name: 'Calvin Coolidge',
    wiki: 'http://en.wikipedia.org/wiki/Calvin_Coolidge',
    tookOffice: '2/8/1923',
    leftOffice: '4/03/1929',
    party: 'Republican '
  },
  {
    first: 'Herbert',
    middle: '',
    last: 'Hoover',
    number: '31',
    name: 'Herbert Hoover',
    wiki: 'http://en.wikipedia.org/wiki/Herbert_Hoover',
    tookOffice: '4/03/1929',
    leftOffice: '4/03/1933',
    party: 'Republican '
  },
  {
    first: 'Franklin',
    middle: 'D.',
    last: 'Roosevelt',
    number: '32',
    name: 'Franklin D. Roosevelt',
    wiki: 'http://en.wikipedia.org/wiki/Franklin_D._Roosevelt',
    tookOffice: '4/03/1933',
    leftOffice: '12/4/1945',
    party: 'Democratic'
  },
  {
    first: 'Harry',
    middle: 'S.',
    last: 'Truman',
    number: '33',
    name: 'Harry S. Truman',
    wiki: 'http://en.wikipedia.org/wiki/Harry_S._Truman',
    tookOffice: '12/4/1945',
    leftOffice: '20/01/1953',
    party: 'Democratic'
  },
  {
    first: 'Dwight',
    middle: 'D.',
    last: 'Eisenhower',
    number: '34',
    name: 'Dwight D. Eisenhower',
    wiki: 'http://en.wikipedia.org/wiki/Dwight_D._Eisenhower',
    tookOffice: '20/01/1953',
    leftOffice: '20/01/1961',
    party: 'Republican '
  },
  {
    first: 'John',
    middle: 'F.',
    last: 'Kennedy',
    number: '35',
    name: 'John F. Kennedy',
    wiki: 'http://en.wikipedia.org/wiki/John_F._Kennedy',
    tookOffice: '20/01/1961',
    leftOffice: '22/11/1963',
    party: 'Democratic'
  },
  {
    first: 'Lyndon',
    middle: 'B.',
    last: 'Johnson',
    number: '36',
    name: 'Lyndon B. Johnson',
    wiki: 'http://en.wikipedia.org/wiki/Lyndon_B._Johnson',
    tookOffice: '22/11/1963',
    leftOffice: '20/1/1969',
    party: 'Democratic'
  },
  {
    first: 'Richard',
    middle: '',
    last: 'Nixon',
    number: '37',
    name: 'Richard Nixon',
    wiki: 'http://en.wikipedia.org/wiki/Richard_Nixon',
    tookOffice: '20/1/1969',
    leftOffice: '9/8/1974',
    party: 'Republican'
  },
  {
    first: 'Gerald',
    middle: '',
    last: 'Ford',
    number: '38',
    name: 'Gerald Ford',
    wiki: 'http://en.wikipedia.org/wiki/Gerald_Ford',
    tookOffice: '9/8/1974',
    leftOffice: '20/01/1977',
    party: 'Republican'
  },
  {
    first: 'Jimmy',
    middle: '',
    last: 'Carter',
    number: '39',
    name: 'Jimmy Carter',
    wiki: 'http://en.wikipedia.org/wiki/Jimmy_Carter',
    tookOffice: '20/01/1977',
    leftOffice: '20/01/1981',
    party: 'Democratic '
  },
  {
    first: 'Ronald',
    middle: '',
    last: 'Reagan',
    number: '40',
    name: 'Ronald Reagan',
    wiki: 'http://en.wikipedia.org/wiki/Ronald_Reagan',
    tookOffice: '20/01/1981',
    leftOffice: '20/01/1989',
    party: 'Republican '
  },
  {
    first: 'George',
    middle: 'H. W.',
    last: 'Bush',
    number: '41',
    name: 'George H. W. Bush',
    wiki: 'http://en.wikipedia.org/wiki/George_H._W._Bush',
    tookOffice: '20/01/1989',
    leftOffice: '20/01/1993',
    party: 'Republican '
  },
  {
    first: 'Bill',
    middle: '',
    last: 'Clinton',
    number: '42',
    name: 'Bill Clinton',
    wiki: 'http://en.wikipedia.org/wiki/Bill_Clinton',
    tookOffice: '20/01/1993',
    leftOffice: '20/01/2001',
    party: 'Democratic '
  },
  {
    first: 'George',
    middle: 'W.',
    last: 'Bush',
    number: '43',
    name: 'George W. Bush',
    wiki: 'http://en.wikipedia.org/wiki/George_W._Bush',
    tookOffice: '20/01/2001',
    leftOffice: '20/01/2009',
    party: 'Republican '
  },
  {
    first: 'Barack',
    middle: '',
    last: 'Obama',
    number: '44',
    name: 'Barack Obama',
    wiki: 'http://en.wikipedia.org/wiki/Barack_Obama',
    tookOffice: '20/01/2009',
    leftOffice: 'Incumbent ',
    party: '  Democratic   '
  }
]
