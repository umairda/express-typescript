import User from '../models/User'
import Event from '../models/Event'

export const generateUser = async (): Promise<boolean> => {
  try {
    const user: User = User.generateRandomUser()
    user.addEvent(new Event({ type: 'SIGN_UP', email: user.email }))
    const saved = await user.save()
    return saved
  } catch (err) {
    return false
  }
}

export const generateEvent = async (): Promise<boolean> => {
  const user: User | null = await User.getRandomUser()

  if (!user) {
    return false
  } else {
    user.addEvent(Event.generateRandomEvent())
    await user.update()
    return true
  }
}

export const generateEventOneDayAgo = async (): Promise<boolean> => {
  const user: User | null = await User.getRandomUser()

  if (!user) {
    return false
  } else {
    const event: Event = Event.generateRandomEvent()
    event.setCreatedToOverADayAgo()
    user.addEvent(event)
    await user.update()
    return true
  }
}

export default (): void => {
  //TODO: determine typing for intervalId
  const intervalId: any = setInterval(async () => {
    await generateUser()
    await generateEvent()
    await generateEventOneDayAgo()
  }, 3000)

  process.on('exit', () => {
    clearInterval(intervalId)
  })
}
