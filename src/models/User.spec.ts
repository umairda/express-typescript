import Event from './Event'
import User from './User'

describe('User', function() {
  it('should set the class properties per the constructor arguments', function() {
    const email = 'test@test.com'
    const password = 'test123'
    const user: User = new User({ email, password })

    expect(user.email).toBe(email)
    expect(user.password).toBe(password)
  })

  it('should NOT set the class phone property if the phone is invalid', function() {
    const email = 'test@test.com'
    const password = 'test123'
    const phone = 'abc-def-ghij'
    const user: User = new User({ email, password, phone })

    expect(user.email).toBe(email)
    expect(user.password).toBe(password)
    expect(user.phone).toBe('')
  })

  it('should set the class phone property if the phone is valid', function() {
    const email = 'test@test.com'
    const password = 'test123'
    const phone = '123-456-7890'
    const user: User = new User({ email, password, phone })

    expect(user.email).toBe(email)
    expect(user.password).toBe(password)
    expect(user.phone).toBe(phone)
  })

  describe('addEvent', function() {
    it('should add an event to the events array', function() {
      const email = 'test@test.com'
      const password = 'test123'
      const user: User = new User({ email, password })

      const type = 'LOGIN'

      const event: Event = new Event({ type })

      expect(user.events.length).toBe(0)

      const totalEvents: number = user.addEvent(event)
      expect(totalEvents).toBe(1)
      expect(user.events.length).toBe(1)
      expect(user.events[0]).toEqual(event)
    })
  })

  describe('save', function() {
    it('should return a promise that resolves to true if the user does not exist', async function() {
      const email = 'test@test.com'
      const password = 'test123'
      const user: User = new User({ email, password })

      let foundUser: User = await User.getUser(email)
      expect(foundUser).toBeNull()

      const saved = await user.save()
      expect(saved).toBeTruthy()

      foundUser = await User.getUser(email)
      expect(foundUser).not.toBeNull()

      const savedAgain = await user.save()
      expect(savedAgain).not.toBeTruthy()
    })
  })

  describe('update', function() {
    it('should update a user and return the current version', async function() {
      const email = 'test1@test.com'
      const password = 'test123'
      const user: User = new User({ email, password })

      const saved = await user.save()
      expect(saved).toBeTruthy()
      expect(user.events.length).toBe(0)

      user.addEvent(new Event({ type: 'LOGIN' }))
      expect(user.events.length).toBe(1)
      expect(user.getVersion()).toBe(1)

      const nextVersion: number = await user.update()
      expect(nextVersion).toBe(2)

      expect(user.getVersion()).toBe(2)

      const foundUser = await User.getUser(email)
      expect(foundUser.events.length).toBe(1)
      expect(foundUser.getVersion()).toBe(2)
    })
  })

  describe('getAllUsers', function() {
    it('should get all the users', async function() {
      await User.clearUsers()
      const user1: User = User.generateRandomUser()
      await user1.save()
      const user2: User = User.generateRandomUser()
      await user2.save()
      const user3: User = User.generateRandomUser()
      await user3.save()

      const users: User[] = [user1, user2, user3]
      const emailAddresses: string[] = users.map((user: User) => user.email)
      const allUsers: User[] = await User.getAllUsers()

      expect(allUsers.length).toBe(3)
      allUsers.forEach((user: User) => {
        expect(emailAddresses.indexOf(user.email) !== -1).toBeTruthy()
      })
    })
  })
})
