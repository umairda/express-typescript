# express / typescript

### Setup

---

```
clone repo
npm install
npm start
```

- starts server on port 8000 (configurable from `src/config/settings.ts`)

### Routes

---

#### List all user events

```
  [GET] /v1/events
```

Sample response:

```
[
  {
    email: "BarackKennedy-1570136511924@test.com",
    type: "SIGN_UP",
    created: 1570136511925
  },
  {
    email: "BarackKennedy-1570136511924@test.com",
    type: "LOGOUT",
    created: 1570136511927
  },
  {
    email: "BarackKennedy-1570136511924@test.com",
    type: "ATTEMPTED_BLACKMAIL",
    created: 1570136514929
  },
  ...
]
```

#### List all user events within one day

```
  [GET] /v1/events-within-one-day
```

Sample response:

```
[
  {
    email: "BarackKennedy-1570136511924@test.com",
    type: "SIGN_UP",
    created: 1570136511925
  },
  {
    email: "BarackKennedy-1570136511924@test.com",
    type: "LOGOUT",
    created: 1570136511927
  },
  {
    email: "BarackKennedy-1570136511924@test.com",
    type: "ATTEMPTED_BLACKMAIL",
    created: 1570136514929
  },
  ...
]
```

#### List all events for a user provided their email address

```
  [GET] /v1/events/:email
```

Sample request:

```
  [GET] /v1/events/ChesterClinton-1570136514929@test.com
```

Sample response:

```
[
  {
    email: "ChesterClinton-1570136514929@test.com",
    type: "SIGN_UP",
    created: 1570136514929
  },
  {
    email: "ChesterClinton-1570136514929@test.com",
    type: "LOGOUT",
    created: 1570136520933
  },
  {
    email: "ChesterClinton-1570136514929@test.com",
    type: "LOGIN",
    created: 1570136523935
  },
  {
    email: "ChesterClinton-1570136514929@test.com",
    type: "SIGN_UP",
    created: 1570136526939
  },
  {
    email: "ChesterClinton-1570136514929@test.com",
    type: "ATTEMPTED_BLACKMAIL",
    created: 1570136656084
  }
]
```

#### Create a user

```
  [POST] /v1/user

  request body:
  {
    email: "test@test.com",
    password: "cleartext-is-bad-but...",
    phone: "optional"
  }
```

Sample response:

```
  true (or false if failed)
```

#### Get a user

```
  [GET] /v1/user/:email
```

Sample response:

```
{
  created: 1570140208547,
  email: "tes1t@test.com",
  password: "***",
  phone: "925-819-6119",
  events: [
    {
      email: "tes1t@test.com",
      type: "SIGN_UP",
      created: 1570140208547
    }
  ],
  version: 1
}
```

#### Create an event for a user

```
  [POST] /v1/user/:email/event

  request body:
  {
    type: "STEAL"
  }
```

Sample response (lists all of user's events):

```
[
    {
        "email": "test@test.com",
        "type": "SIGN_UP",
        "created": 1570139390061
    },
    {
        "email": "test@test.com",
        "type": "SIGN_UP",
        "created": 1570139395416
    },
    {
        "email": "test@test.com",
        "type": "SIGN_UP",
        "created": 1570049401425
    },
    {
        "email": "test@test.com",
        "type": "FORGED_DRIVERS_LICENSE",
        "created": 1570049422442
    },
    {
        "email": "test@test.com",
        "type": "ATTEMPTED_BLACKMAIL",
        "created": 1570139428450
    },
    {
        "email": "test@test.com",
        "type": "ATTEMPTED_BLACKMAIL",
        "created": 1570139437454
    },
    {
        "email": "test@test.com",
        "type": "STEAL",
        "created": 1570139510344
    }
]
```

---

### Notes

- A random user/event generator runs in the background and generates a new user, a new event for a random user, and a new event for a random user that occurred a day ago

- Convenience routes are provided to `create` and manually add `events` for users for testing
