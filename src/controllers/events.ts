import { Request, Response } from 'express'
import Event from '../models/Event'
import User from '../models/User'

export const listAllEvents = async (req: Request, res: Response): Promise<void> => {
  try {
    const allUsers: User[] = await User.getAllUsers()
    const allEvents: Event[] = allUsers.reduce((p: Event[], u: User) => [...p, ...u.events], [])
    res.json(allEvents)
  } catch (err) {
    res.sendStatus(500)
  }
}

export const listAllEventsWithinOneDay = async (req: Request, res: Response): Promise<void> => {
  try {
    const allUsers: User[] = await User.getAllUsers()
    const allEvents: Event[] = allUsers.reduce((p: Event[], u: User) => [...p, ...u.events], [])
    const oneDayAgo: number = Date.now() - 24 * 3600 * 1000
    const allEventsWithinOneDay: Event[] = allEvents.filter((event: Event) => event.getCreated() >= oneDayAgo)
    res.json(allEventsWithinOneDay)
  } catch (err) {
    res.sendStatus(500)
  }
}

export const listUserEvents = async (req: Request, res: Response): Promise<void> => {
  try {
    const foundUser: User = await User.getUser(req.params.email)
    const events: Event[] = foundUser ? foundUser.events : []
    res.json(events)
  } catch (err) {
    res.sendStatus(500)
  }
}
